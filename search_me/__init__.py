from .api import Searx, Google, Rambler

__author__ = "Michael R. Kisel"
__license__ = "MIT"
__version__ = "2.1"
__maintainer__ = "Michael R. Kisel"
__email__ = "deploy-me@yandex.ru"
__status__ = "Stable"
